---
redirect_from:
  - /_pages/Gallery/undirected/softmaint.html
layout: gallery
title: Module Dependencies
svg: softmaint.svg
copyright: Copyright &#169; 1996 AT&amp;T.  All rights reserved.
gv_file: softmaint.gv.txt
img_src: softmaint.png
---
The graph represents dependencies between modifications to
a large program.  Because testing such programs is difficult
and expensive, the graph was made to discover which
subsets of modifications might be tested separately by
understanding or even eliminating a few key dependencies.
