---
name: shape
---
A string specifying the [shape](shapes.html) of a node. There are three main
types of shapes:

* [polygon-based](shapes.html#polygon),
* [record-based](shapes.html#record) and
* [user-defined](shapes.html#epsf).

The record-based shape has largely been superseded and greatly generalized by
[HTML-like labels](shapes.html#html). That is, instead of using `shape=record`,
consider using `shape=none` and an HTML-like label.
