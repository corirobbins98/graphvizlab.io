---
defaults:
- black
flags: []
minimums: []
name: labelfontcolor
types:
- color
used_by: E
---
Color used for [`headlabel`](#d:headlabel) and [`taillabel`](#d:taillabel).

If not set, defaults to edge's [`fontcolor`](#d:fontcolor).
