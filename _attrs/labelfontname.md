---
defaults:
- '"Times-Roman"'
flags: []
minimums: []
name: labelfontname
types:
- string
used_by: E
---
Font used for [`headlabel`](#d:headlabel) and [`taillabel`](#d:taillabel).

If not set, defaults to edge's [`fontname`](#d:fontname).
