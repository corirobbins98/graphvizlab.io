---
name: GIF
params:
- gif
---
Outputs GIF bitmap images.
